#!/usr/bin/env python

"""
For any given unix directory, print out a tree structure of all its contents.
Example:
$ python tree.py mydir
mydir
 |---subdir1/
 |
 |---subdir2/
 |   |--subfile1.jpg
 |   |--sublink -> subfile2.jpg
 |
 |---file1.txt
 |---file2.txt
 |___lastfile.txt
"""
# from pygments import highlight
# from pygments.lexers import BashLexer, BashSessionLexer
# from pygments.formatters import Terminal256Formatter

import os
import os.path
import logging
import optparse
import traceback, sys

"""
General Algorithm:
    1) Scan all the elements in the current directory (or the ones in the arguments)
    2) Categorize them on the basis of their type (file, directory, symlink, others, etc).
    3) Arrange them according to a given parameter (callable, so that can be changed based on the given switch)
        (SIDE NOTE: The callables should probably also be assigned priority, for cases where more than one switch is given)
    4) Create the final List.
    5) Take that list and arrange it into the form of a tree structure.
    6) If any particular item is of type directory, recursively call the function to walk through it as well.
    7) Print out the final tree output.
"""

def scan(arg, dirname, names):
    """
    If a command line argument is given, list it's contents, otherwise
    list the contents of the directory from which the command is executed.
    """
    if len(names) == 0:
        pass
    else:
        #print 'names: ', names
        print '%s/' %dirname
    for name in names:
        subname = os.path.join(dirname, name)
        if subname[0] == '.' and subname[1] == '/':
            subname = subname[2:]
        #print 'subname: ', subname
        if os.path.isdir(subname):
            #print '      %s/' %name
            pass
        elif os.path.islink(subname):
            print '      %s -> %s' %(subname, os.readlink(subname))
        elif os.path.ismount(subname):
            print '      %s -> Mount' %name
        elif os.path.isfile(subname):
            print '      %s' %name
    #print

def printf(dirname, names, sort=None):
    """
    Print the tree. If sort is not None, print after sorting it.
    """
    pass 

def get_contents(path):
    return os.listdir(path)

def colorify(string):
    print highlight(string, BashLexer(), Terminal256Formatter())

def print_tree(directory, padding=1):
    """Print the tree for a given directory"""
    try:
        content = get_contents(directory)
        for f in content:
            path = directory + '/' + f 
            abspath = os.path.abspath(path)
            if os.path.exists(abspath):
                #print 'abspath - ', abspath
                #print os.path.islink(abspath)
                if os.path.islink(abspath):
                    print "%s___%s -> %s" %('|'.rjust(padding), os.path.basename(abspath), os.readlink(abspath).rjust(padding+5))
                elif os.path.islink(abspath):
                    print "%s___%s -> MountPoint" %('|'.rjust(padding), os.path.basename(abspath))
                elif os.path.isfile(abspath):
                    print "%s___%s" %('|'.rjust(padding), os.path.basename(abspath))
                elif os.path.isdir(abspath):
                    print_tree(abspath, padding=padding+4)     # Recurse
    except Exception as e:
        logging.error("Not a directory!")
        traceback.print_exc(file=sys.stdout)

def loop_dir(list_dir):
    """print the tree for a list of directories"""
    for d in list_dir:
        print os.path.basename(d)
        print_tree(d, padding=1)

def main(args):
    if len(args) > 1:
        loop_dir(args[1:])
    else:
        loop_dir(['.'])

if __name__ == '__main__':
    import sys
    main(sys.argv)
