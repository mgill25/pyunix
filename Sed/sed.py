#!/usr/bin/env python
# -*- coding: utf-8 -*-

# A Basic Sed
#TODO 1. Proper Argument Parsing and handling multiple options
#TODO 2. Work with multiple files/directories
#TODO 3. Use fnmatch to match files/directories with unix regex
#TODO 4. Can we use Unix-style regex for matching and substituting as well?

import os
import sys
import re
import shutil
from tempfile import mkstemp


def _find_and_replace(file_list, find_pattern, replace_string):
    """
    For all the files in the file_path, replace all instances
    of the string that matches find_pattern with the replace_string.
    file_list: List of os.abspath() files
    We use the tempfile module to make a temporary file and write to it
    here, since modifying a file in-place is not a good idea.
    """
    try:
        for f in file_list:
            # create a temp file
            fn, temp_abs = mkstemp()
            with open(f, 'r') as infile, open(temp_abs, 'w') as outfile:
                old_content = infile.read()
                if re.match(find_pattern, old_content):
                    new_content = re.sub(find_pattern, replace_string, old_content)
                else:
                    raise RuntimeError("No match found!")
                outfile.write(new_content)
                shutil.move(temp_abs, f)
    except Exception, err:
        print("%s" % err)


def find_and_replace(path, find_pattern, replace_string):
    """
    path: command line arguments, can be a file or directory.
    """
    path = os.path.abspath(path)
    if os.path.isdir(path):
        file_list = [item for item in os.listdir(path) if os.path.isfile(item)]
        _find_and_replace(file_list, find_pattern, replace_string)
    elif os.path.isfile(path):
        _find_and_replace([path], find_pattern, replace_string)

if __name__ == '__main__':
    if len(sys.argv) < 1:
        print("No path specified!")
    elif len(sys.argv) > 1 and (sys.argv[1] == "--help" or sys.argv[1] == "-h"):
        print("Usage: python sed.py <path> <find-string> <replace-string>")
    elif len(sys.argv) > 1:
        find_and_replace(sys.argv[1], sys.argv[2], sys.argv[3])
